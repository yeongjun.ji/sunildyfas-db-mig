﻿
namespace sunilDBmig
{
    partial class Qmsset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.btnQmsset = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtpassword = new System.Windows.Forms.TextBox();
            this.txtid = new System.Windows.Forms.TextBox();
            this.txtsname = new System.Windows.Forms.TextBox();
            this.txtport = new System.Windows.Forms.TextBox();
            this.txthost = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(82, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(181, 24);
            this.label6.TabIndex = 23;
            this.label6.Text = "QMS 접속 설정";
            // 
            // btnQmsset
            // 
            this.btnQmsset.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnQmsset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQmsset.Font = new System.Drawing.Font("굴림", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnQmsset.ForeColor = System.Drawing.SystemColors.Control;
            this.btnQmsset.Location = new System.Drawing.Point(232, 260);
            this.btnQmsset.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnQmsset.Name = "btnQmsset";
            this.btnQmsset.Size = new System.Drawing.Size(72, 31);
            this.btnQmsset.TabIndex = 34;
            this.btnQmsset.Text = "적용";
            this.btnQmsset.UseVisualStyleBackColor = false;
            this.btnQmsset.Click += new System.EventHandler(this.btnQmsset_Click_1);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(68, 218);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 15);
            this.label5.TabIndex = 33;
            this.label5.Text = "Password =";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(87, 187);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 15);
            this.label4.TabIndex = 32;
            this.label4.Text = "User Id =";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 15);
            this.label3.TabIndex = 31;
            this.label3.Text = "SERVICE_NAME=";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(93, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 15);
            this.label2.TabIndex = 30;
            this.label2.Text = "PORT =";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(93, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 15);
            this.label1.TabIndex = 29;
            this.label1.Text = "HOST =";
            // 
            // txtpassword
            // 
            this.txtpassword.Location = new System.Drawing.Point(154, 213);
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.Size = new System.Drawing.Size(150, 25);
            this.txtpassword.TabIndex = 28;
            // 
            // txtid
            // 
            this.txtid.Location = new System.Drawing.Point(154, 182);
            this.txtid.Name = "txtid";
            this.txtid.Size = new System.Drawing.Size(150, 25);
            this.txtid.TabIndex = 27;
            // 
            // txtsname
            // 
            this.txtsname.Location = new System.Drawing.Point(154, 151);
            this.txtsname.Name = "txtsname";
            this.txtsname.Size = new System.Drawing.Size(150, 25);
            this.txtsname.TabIndex = 26;
            // 
            // txtport
            // 
            this.txtport.Location = new System.Drawing.Point(154, 120);
            this.txtport.Name = "txtport";
            this.txtport.Size = new System.Drawing.Size(150, 25);
            this.txtport.TabIndex = 25;
            // 
            // txthost
            // 
            this.txthost.Location = new System.Drawing.Point(154, 89);
            this.txthost.Name = "txthost";
            this.txthost.Size = new System.Drawing.Size(150, 25);
            this.txthost.TabIndex = 24;
            // 
            // Qmsset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 319);
            this.Controls.Add(this.btnQmsset);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtpassword);
            this.Controls.Add(this.txtid);
            this.Controls.Add(this.txtsname);
            this.Controls.Add(this.txtport);
            this.Controls.Add(this.txthost);
            this.Controls.Add(this.label6);
            this.Name = "Qmsset";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Qmsset";
            this.Load += new System.EventHandler(this.Qmsset_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnQmsset;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtpassword;
        private System.Windows.Forms.TextBox txtid;
        private System.Windows.Forms.TextBox txtsname;
        private System.Windows.Forms.TextBox txtport;
        private System.Windows.Forms.TextBox txthost;
    }
}