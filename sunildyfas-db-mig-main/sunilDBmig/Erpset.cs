﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sunilDBmig
{
    public partial class Erpset : Form
    {
        public Erpset()
        {
            InitializeComponent();
        }

        private void btnErpset_Click_1(object sender, EventArgs e)
        {
            IniFile ini = new IniFile();
            ini["ERP setting"]["HOST"] = txthost.Text;
            ini["ERP Setting"]["PORT"] = txtport.Text;
            ini["ERP Setting"]["SERVICE_NAME"] = txtsname.Text;
            ini["ERP Setting"]["User id"] = txtid.Text;
            ini["ERP Setting"]["Password"] = txtpassword.Text;
            ini.Save(@"C:\inifile\ERP.ini");
            try
            {
                MessageBox.Show("ERP setting 완료");
                this.Close();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Erpset_Load(object sender, EventArgs e)
        {
            IniFile ini = new IniFile();
            ini.Load(@"C:\inifile\ERP.ini");
            string ERPhost = ini["ERP setting"]["HOST"].ToString();
            string ERPport = ini["ERP Setting"]["PORT"].ToString();
            string ERPsname = ini["ERP Setting"]["SERVICE_NAME"].ToString();
            string ERPid = ini["ERP Setting"]["User Id"].ToString();
            string ERPPassword = ini["ERP Setting"]["Password"].ToString();

            txthost.Text = ERPhost;
            txtport.Text = ERPport;
            txtsname.Text = ERPsname;
            txtid.Text = ERPid;
            txtpassword.Text = ERPPassword;
        }
    }
}
