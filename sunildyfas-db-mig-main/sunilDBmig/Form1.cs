﻿using MySql.Data.MySqlClient;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using Z.BulkOperations;
/*****************************************************백업용 코드********************************************************/
namespace sunilDBmig
{
    public partial class Form1 : Form
    {
        DataSet ds = new DataSet();
        private OracleDataAdapter dataAdapter = new OracleDataAdapter();
        progform newprogform = new progform();
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            // inifile 폴더, 임시 inifile생성
            string popinifolder = @"C:\popinifile";
            string erpinifolder = @"C:\eprinifile";
            string qmsinifolder = @"C:\qmsinifile";
            string csvfolder = @"C:\csvfile";
            if (!Directory.Exists(popinifolder) || !Directory.Exists(erpinifolder) || Directory.Exists(qmsinifolder) || !Directory.Exists(csvfolder))
            {
                Directory.CreateDirectory(popinifolder);
                Directory.CreateDirectory(erpinifolder);
                Directory.CreateDirectory(qmsinifolder);
                Directory.CreateDirectory(csvfolder);
            }

            // 접속정보 설정
            IniFile popini = new IniFile();
            popini.Load(@"C:\inifile\POP.ini");
            string POPhost = popini["POP setting"]["HOST"].ToString();
            string POPport = popini["POP Setting"]["PORT"].ToString();
            string POPsname = popini["POP Setting"]["SERVICE_NAME"].ToString();
            string POPid = popini["POP Setting"]["User Id"].ToString();
            string POPPassword = popini["POP Setting"]["Password"].ToString();

            // Oracle DB
            string Oconn = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + POPhost + ")(PORT=" + POPport +
                            ")))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + POPsname + ")));User Id=" + POPid + ";Password=" + POPPassword;
            OracleConnection ocon = new OracleConnection(Oconn);

            // Recive DB
            string Rconn = "Server=66.232.147.84;Port=3307;Database=standard_db;Uid=moornmo_edu;Pwd=edu1234;SSLMode=None;";
            MySqlConnection rcon = new MySqlConnection(Rconn);

            // Send DB
            string Sconn = "Server=3.35.115.221;Port=3306;Database=sunildyfas_db;Uid=root;Pwd=anfmsah12#;SSLMode=None;";
            MySqlConnection scon = new MySqlConnection(Sconn);

            try
            {
                MessageBox.Show("모든 DB접속 가능여부 확인중...  (확인을 누르세요)");
                ocon.Open();
                rcon.Open();
                scon.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            ocon.Close();
            rcon.Close();
            scon.Close();
        }

        /****************************** 수동 조회버튼 ******************************/
        private void btn수동전송_Click(object sender, EventArgs e)
        {
            progressBar1.Visible = true;
            txt데이터전송.Visible = true;

            DataMigration_datatable();
        }


        /****************************** 비동기화 ******************************/
        private async void DataMigration_datatable() // DATATABLE 전송방식
        {
            progressBar1.Visible = true;
            txt데이터전송.Visible = true;
            string strDate1 = dateTimePicker1.Value.ToString("yyyyMMdd");
            string strDate2 = dateTimePicker2.Value.ToString("yyyyMMdd");
            string strwow1 = strDate1;
            string strwow2 = strDate2;

            var task1 = Task.Run(() => DEL_SF_PROD_RECORD("DELETE FROM sunil_pop_sf_prod_record WHERE REG_DATE BETWEEN '" + strwow1 + "' AND '" + strwow2 + "'"));
            await task1;
            var task2 = Task.Run(() => SF_PROD_RECORD("SELECT * FROM POP.SF_PROD_RECORD WHERE REG_DATE BETWEEN TO_DATE('" + strwow1 + "', 'YYYYMMDD') AND TO_DATE('" + strwow2 + "')"));
            await task2;
            progressBar1.Visible = false;
            txt데이터전송.Visible = false;
        }




        private async void DataMigration_csv() // CSV 전송방식
        {
            string strDate1 = dateTimePicker1.Value.ToString("yyyyMMdd");
            string strDate2 = dateTimePicker2.Value.ToString("yyyyMMdd");
            string strwow1 = strDate1;
            string strwow2 = strDate2;

            var task1 = Task.Run(() => DEL_SF_PROD_RECORD("DELETE FROM sunil_pop_sf_prod_record WHERE REG_DATE BETWEEN '" + strwow1 + "' AND '" + strwow2 + "'"));
            await task1;
            var task2 = Task.Run(() => getcsvfile("SELECT * FROM POP.SF_PROD_RECORD WHERE REG_DATE BETWEEN TO_DATE('" + strwow1 + "', 'YYYYMMDD') AND TO_DATE('" + strwow2 + "')"));
            await task2;
            StreamWriter sw = new StreamWriter(@"C:\csvfile\test.csv"); // C:\csvfile\test.csv에 저장
            var task3 = Task.Run(() => WriteToStream(sw, ds.Tables[0], true, false));
            await task3;
            var task4 = Task.Run(() => MySqlBulkLoaderAsync(@"C:\csvfile\test.csv"));
            await task4;
            progressBar1.Visible = false;
            txt데이터전송.Visible = false;
        }

        /****************************** DATATABLE 전송 Method (테이블별 모듈)******************************/
        private void SF_PROD_RECORD(string selectCommand)
        {
            IniFile ini = new IniFile();
            ini.Load(@"C:\inifile\POP.ini");
            string POPhost = ini["POP setting"]["HOST"].ToString();
            string POPport = ini["POP Setting"]["PORT"].ToString();
            string POPsname = ini["POP Setting"]["SERVICE_NAME"].ToString();
            string POPid = ini["POP Setting"]["User Id"].ToString();
            string POPPassword = ini["POP Setting"]["Password"].ToString();
            // Oracle DB
            string Oconn = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + POPhost + ")(PORT=" + POPport +
                             ")))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + POPsname + ")));User Id=" + POPid + ";Password=" + POPPassword;
            OracleConnection ocon = new OracleConnection(Oconn);
            try
            {
                dataAdapter = new OracleDataAdapter(selectCommand, ocon);

                OracleCommandBuilder commandBuilder = new OracleCommandBuilder(dataAdapter);

                DataTable table = new DataTable
                {
                    Locale = CultureInfo.InvariantCulture
                };
                ocon.Open();
                dataAdapter.Fill(table);
                //dataAdapter.UpdateBatchSize = 0;
                //dataAdapter.Update(table);
                ocon.Close();
                string SConn = "Server=3.35.115.221;Port=3306;Database=sunildyfas_db;Uid=root;Pwd=anfmsah12#;SSLMode=None;";
                //string SConn = "Server=66.232.147.84;Port=3307;Database=standard_db;Uid=moornmo_edu;Pwd=edu1234;SSLMode=None;";
                using (MySqlConnection sconn = new MySqlConnection(SConn))
                {
                    sconn.Open();
                    MySqlTransaction tran = sconn.BeginTransaction(); // 트랜젝션 시작

                    using (var bulk = new BulkOperation(sconn))
                    {
                        bulk.Transaction = tran;
                        foreach (DataColumn c in table.Columns)
                            bulk.ColumnMappings.Add(c.ColumnName, c.ColumnName);
                        //bulk.DestinationTableName = "test";
                        bulk.DestinationTableName = "sunil_pop_sf_prod_record";
                        try
                        {
                            bulk.BatchTimeout = 0;
                            bulk.BulkInsert(table);
                            tran.Commit(); // 트랜잭션 커밋
                            sconn.Close();
                            MessageBox.Show("DATA MIGRATION SUCCESS", "완료");
                        }
                        catch (Exception ex)
                        {
                            tran.Rollback();
                            MessageBox.Show(ex.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void DEL_SF_PROD_RECORD(string deleteCommand)
        {
            //string setPacket = "SET GLOBAL max_allowed_packet = 1024 * 1024 * 64;";
            string Deletequery = deleteCommand;
            //string Conn = "Server=66.232.147.84;Port=3307;Database=standard_db;Uid=moornmo_edu;Pwd=edu1234;SSLMode=None;";
            string Conn = "Server=3.35.115.221;Port=3306;Database=sunildyfas_db;Uid=root;Pwd=anfmsah12#;SSLMode=None;";
            using (MySqlConnection con = new MySqlConnection(Conn))
            {
                con.Open();

                MySqlTransaction tran = con.BeginTransaction(); // 트랜젝션 시작
                MySqlCommand cmd = new MySqlCommand();

                cmd.Connection = con;
                cmd.Transaction = tran;

                try
                {
                    //cmd.CommandText = setPacket; // set packet 쿼리지정
                    //cmd.ExecuteNonQuery(); // 실행
                    cmd.CommandText = Deletequery; // delete 쿼리지정
                    cmd.ExecuteNonQuery(); // 실행                         

                    tran.Commit(); // 트랜잭션 커밋
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    MessageBox.Show(ex.ToString());
                }
            }
        }


        /****************************** 설정버튼 method ******************************/
        private void button2_Click(object sender, EventArgs e)
        {
            Erpset newErpset = new Erpset();
            newErpset.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Popset newPopset = new Popset();
            newPopset.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Qmsset newQmsset = new Qmsset();
            newQmsset.ShowDialog();
        }


















        /****************************** CSV FILE 전송방식 Method ******************************/

        // csv파일 생성
        private void getcsvfile(string selectCommand)
        {
            progressBar1.Visible = true;
            txt데이터전송.Visible = true;
            IniFile ini = new IniFile();
            ini.Load(@"C:\inifile\POP.ini");
            string POPhost = ini["POP setting"]["HOST"].ToString();
            string POPport = ini["POP Setting"]["PORT"].ToString();
            string POPsname = ini["POP Setting"]["SERVICE_NAME"].ToString();
            string POPid = ini["POP Setting"]["User Id"].ToString();
            string POPPassword = ini["POP Setting"]["Password"].ToString();
            // Oracle DB
            string Oconn = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" + POPhost + ")(PORT=" + POPport +
                             ")))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" + POPsname + ")));User Id=" + POPid + ";Password=" + POPPassword;
            using (OracleConnection ocon = new OracleConnection(Oconn))
            {
                //I assume you know better what is your connection string

                OracleDataAdapter adapter = new OracleDataAdapter(selectCommand, ocon);


                string selectquery = selectCommand;
                OracleCommand cmd = new OracleCommand(selectquery, ocon);
                cmd.Connection = ocon;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 0;

                OracleDataAdapter da = new OracleDataAdapter(cmd);
                try
                {

                    ocon.Open();
                    da.Fill(ds);
                    ocon.Close();
                    txt데이터전송.Visible = false;
                    progressBar1.Visible = false;
                }
                catch (System.Exception ex)
                {
                    txt데이터전송.Visible = false;
                    progressBar1.Visible = false;
                    MessageBox.Show(ex.ToString());
                }
                finally
                {
                    cmd.Dispose();
                    ocon.Close();
                }
            }
        }
        public void MySqlBulkLoaderAsync(string csvFilePath)
        {
            try
            {
                using (var conn = new MySqlConnection("Server=3.35.115.221;Port=3306;Database=sunildyfas_db;Uid=root;Pwd=anfmsah12#;SSLMode=None;CharSet = utf8" + ";AllowLoadLocalInfile=True"))
                //using (var conn = new MySqlConnection("Server=66.232.147.84;Port=3307;Database=standard_db;Uid=moornmo_edu;Pwd=edu1234;SSLMode=None;" + ";AllowLoadLocalInfile=True"))
                {
                    conn.Open();

                    MySqlTransaction tran = conn.BeginTransaction(); // 트랜젝션 시작
                    var bl = new MySqlBulkLoader(conn)
                    {
                        Local = true,
                        TableName = "sunil_pop_sf_prod_record",
                        //TableName = "sunil_pop_sf_prod_record",
                        Timeout = 0,
                        FieldTerminator = ",",
                        LineTerminator = "\r\n",
                        FileName = csvFilePath,
                        NumberOfLinesToSkip = 1,
                        //CharacterSet = "utf8",
                    };
                    int count = bl.Load();
                    txt데이터전송.Visible = false;
                    progressBar1.Visible = false;
                    MessageBox.Show(count + "lines uploaded");
                    conn.Close();
                    MessageBox.Show("DATA 전송 성공");

                }
                System.IO.File.Delete(csvFilePath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                //result = false;
                throw;
            }
        }
        public static void WriteToStream(TextWriter stream, DataTable table, bool header, bool quoteall)
        {
            if (header)
            {
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    WriteItem(stream, table.Columns[i].Caption, quoteall);
                    if (i < table.Columns.Count - 1)
                        stream.Write(',');
                    else
                        stream.Write("\r\n");
                }
            }
            foreach (DataRow row in table.Rows)
            {
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    WriteItem(stream, row[i], quoteall);
                    if (i < table.Columns.Count - 1)
                        stream.Write(',');
                    else
                        stream.Write("\r\n");
                }
            }
            stream.Flush();
            stream.Close();
        }

        private static void WriteItem(TextWriter stream, object item, bool quoteall)
        {
            if (item == null)
                return;
            string s = item.ToString();
            if (quoteall || s.IndexOfAny("\",\x0A\x0D".ToCharArray()) > -1)
                stream.Write("\"" + s.Replace("\"", "\"\"") + "\"");
            else
                stream.Write(s);
            stream.Flush();
        }
    }

}

