﻿
namespace sunilDBmig
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.btn수동전송 = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.listView2 = new System.Windows.Forms.ListView();
            this.listView1 = new System.Windows.Forms.ListView();
            this.listView3 = new System.Windows.Forms.ListView();
            this.btnErpset = new System.Windows.Forms.Button();
            this.BtnPopset = new System.Windows.Forms.Button();
            this.BtnQmsset = new System.Windows.Forms.Button();
            this.txt데이터전송 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CalendarFont = new System.Drawing.Font("맑은 고딕", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dateTimePicker1.CustomFormat = "";
            this.dateTimePicker1.Font = new System.Drawing.Font("맑은 고딕", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(30, 26);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(117, 27);
            this.dateTimePicker1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(151, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "~";
            // 
            // btn수동전송
            // 
            this.btn수동전송.BackColor = System.Drawing.Color.RoyalBlue;
            this.btn수동전송.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn수동전송.Font = new System.Drawing.Font("맑은 고딕", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn수동전송.ForeColor = System.Drawing.SystemColors.Control;
            this.btn수동전송.Location = new System.Drawing.Point(315, 20);
            this.btn수동전송.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn수동전송.Name = "btn수동전송";
            this.btn수동전송.Size = new System.Drawing.Size(108, 36);
            this.btn수동전송.TabIndex = 3;
            this.btn수동전송.Text = "수동 전송";
            this.btn수동전송.UseVisualStyleBackColor = false;
            this.btn수동전송.Click += new System.EventHandler(this.btn수동전송_Click);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.CalendarFont = new System.Drawing.Font("맑은 고딕", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dateTimePicker2.CustomFormat = "";
            this.dateTimePicker2.Font = new System.Drawing.Font("맑은 고딕", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(179, 26);
            this.dateTimePicker2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(117, 27);
            this.dateTimePicker2.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(74, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 21);
            this.label2.TabIndex = 5;
            this.label2.Text = "선일다이파스 ERP";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(466, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 21);
            this.label3.TabIndex = 6;
            this.label3.Text = "선일다이파스 POP";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(848, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(148, 21);
            this.label4.TabIndex = 7;
            this.label4.Text = "선일다이파스 QMS";
            // 
            // listView2
            // 
            this.listView2.HideSelection = false;
            this.listView2.Location = new System.Drawing.Point(403, 130);
            this.listView2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(356, 423);
            this.listView2.TabIndex = 15;
            this.listView2.UseCompatibleStateImageBehavior = false;
            // 
            // listView1
            // 
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(792, 130);
            this.listView1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(356, 423);
            this.listView1.TabIndex = 16;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // listView3
            // 
            this.listView3.HideSelection = false;
            this.listView3.Location = new System.Drawing.Point(10, 130);
            this.listView3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listView3.Name = "listView3";
            this.listView3.Size = new System.Drawing.Size(356, 423);
            this.listView3.TabIndex = 17;
            this.listView3.UseCompatibleStateImageBehavior = false;
            // 
            // btnErpset
            // 
            this.btnErpset.BackColor = System.Drawing.Color.DimGray;
            this.btnErpset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnErpset.Font = new System.Drawing.Font("맑은 고딕", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnErpset.ForeColor = System.Drawing.SystemColors.Control;
            this.btnErpset.Location = new System.Drawing.Point(230, 88);
            this.btnErpset.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnErpset.Name = "btnErpset";
            this.btnErpset.Size = new System.Drawing.Size(74, 30);
            this.btnErpset.TabIndex = 18;
            this.btnErpset.Text = "설정";
            this.btnErpset.UseVisualStyleBackColor = false;
            this.btnErpset.Click += new System.EventHandler(this.button2_Click);
            // 
            // BtnPopset
            // 
            this.BtnPopset.BackColor = System.Drawing.Color.DimGray;
            this.BtnPopset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnPopset.Font = new System.Drawing.Font("맑은 고딕", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.BtnPopset.ForeColor = System.Drawing.SystemColors.Control;
            this.BtnPopset.Location = new System.Drawing.Point(626, 88);
            this.BtnPopset.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnPopset.Name = "BtnPopset";
            this.BtnPopset.Size = new System.Drawing.Size(74, 30);
            this.BtnPopset.TabIndex = 19;
            this.BtnPopset.Text = "설정";
            this.BtnPopset.UseVisualStyleBackColor = false;
            this.BtnPopset.Click += new System.EventHandler(this.button3_Click);
            // 
            // BtnQmsset
            // 
            this.BtnQmsset.BackColor = System.Drawing.Color.DimGray;
            this.BtnQmsset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnQmsset.Font = new System.Drawing.Font("맑은 고딕", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.BtnQmsset.ForeColor = System.Drawing.SystemColors.Control;
            this.BtnQmsset.Location = new System.Drawing.Point(1013, 88);
            this.BtnQmsset.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnQmsset.Name = "BtnQmsset";
            this.BtnQmsset.Size = new System.Drawing.Size(74, 30);
            this.BtnQmsset.TabIndex = 20;
            this.BtnQmsset.Text = "설정";
            this.BtnQmsset.UseVisualStyleBackColor = false;
            this.BtnQmsset.Click += new System.EventHandler(this.button4_Click);
            // 
            // txt데이터전송
            // 
            this.txt데이터전송.AutoSize = true;
            this.txt데이터전송.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txt데이터전송.Location = new System.Drawing.Point(1013, 566);
            this.txt데이터전송.Name = "txt데이터전송";
            this.txt데이터전송.Size = new System.Drawing.Size(96, 15);
            this.txt데이터전송.TabIndex = 28;
            this.txt데이터전송.Text = "데이터 전송중 ...";
            this.txt데이터전송.Visible = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(1016, 584);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(134, 18);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.progressBar1.TabIndex = 29;
            this.progressBar1.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 60000;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1158, 608);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.txt데이터전송);
            this.Controls.Add(this.BtnQmsset);
            this.Controls.Add(this.BtnPopset);
            this.Controls.Add(this.btnErpset);
            this.Controls.Add(this.listView3);
            this.Controls.Add(this.listView1);
            this.Controls.Add(this.listView2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.btn수동전송);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateTimePicker1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "선일다이파스 데이터베이스 전송 프로그램";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn수동전송;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ListView listView3;
        private System.Windows.Forms.Button btnErpset;
        private System.Windows.Forms.Button BtnPopset;
        private System.Windows.Forms.Button BtnQmsset;
        private System.Windows.Forms.Label txt데이터전송;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Timer timer1;
    }
}

