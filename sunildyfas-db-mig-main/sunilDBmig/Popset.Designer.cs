﻿
namespace sunilDBmig
{
    partial class Popset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txthost = new System.Windows.Forms.TextBox();
            this.txtport = new System.Windows.Forms.TextBox();
            this.txtsname = new System.Windows.Forms.TextBox();
            this.txtid = new System.Windows.Forms.TextBox();
            this.txtpassword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnPopset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txthost
            // 
            this.txthost.Location = new System.Drawing.Point(147, 82);
            this.txthost.Name = "txthost";
            this.txthost.Size = new System.Drawing.Size(150, 25);
            this.txthost.TabIndex = 0;
            // 
            // txtport
            // 
            this.txtport.Location = new System.Drawing.Point(147, 113);
            this.txtport.Name = "txtport";
            this.txtport.Size = new System.Drawing.Size(150, 25);
            this.txtport.TabIndex = 1;
            // 
            // txtsname
            // 
            this.txtsname.Location = new System.Drawing.Point(147, 144);
            this.txtsname.Name = "txtsname";
            this.txtsname.Size = new System.Drawing.Size(150, 25);
            this.txtsname.TabIndex = 2;
            // 
            // txtid
            // 
            this.txtid.Location = new System.Drawing.Point(147, 175);
            this.txtid.Name = "txtid";
            this.txtid.Size = new System.Drawing.Size(150, 25);
            this.txtid.TabIndex = 3;
            // 
            // txtpassword
            // 
            this.txtpassword.Location = new System.Drawing.Point(147, 206);
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.Size = new System.Drawing.Size(150, 25);
            this.txtpassword.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(86, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "HOST =";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(86, 118);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "PORT =";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 149);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "SERVICE_NAME=";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(80, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 15);
            this.label4.TabIndex = 8;
            this.label4.Text = "User Id =";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(61, 211);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 15);
            this.label5.TabIndex = 9;
            this.label5.Text = "Password =";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("굴림", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(93, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(178, 24);
            this.label6.TabIndex = 10;
            this.label6.Text = "POP 접속 설정";
            // 
            // btnPopset
            // 
            this.btnPopset.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnPopset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPopset.Font = new System.Drawing.Font("굴림", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnPopset.ForeColor = System.Drawing.SystemColors.Control;
            this.btnPopset.Location = new System.Drawing.Point(225, 253);
            this.btnPopset.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnPopset.Name = "btnPopset";
            this.btnPopset.Size = new System.Drawing.Size(72, 31);
            this.btnPopset.TabIndex = 12;
            this.btnPopset.Text = "적용";
            this.btnPopset.UseVisualStyleBackColor = false;
            this.btnPopset.Click += new System.EventHandler(this.btnPopset_Click);
            // 
            // Popset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(356, 305);
            this.Controls.Add(this.btnPopset);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtpassword);
            this.Controls.Add(this.txtid);
            this.Controls.Add(this.txtsname);
            this.Controls.Add(this.txtport);
            this.Controls.Add(this.txthost);
            this.Name = "Popset";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Popset";
            this.Load += new System.EventHandler(this.Popset_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txthost;
        private System.Windows.Forms.TextBox txtport;
        private System.Windows.Forms.TextBox txtsname;
        private System.Windows.Forms.TextBox txtid;
        private System.Windows.Forms.TextBox txtpassword;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnPopset;
    }
}