﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace sunilDBmig
{
    public partial class Popset : Form
    {
        public Popset()
        {
            InitializeComponent();
        }

        private void btnPopset_Click(object sender, EventArgs e)
        {
            // ini 쓰기
            IniFile ini = new IniFile();
            ini["POP setting"]["HOST"] = txthost.Text;
            ini["POP Setting"]["PORT"] = txtport.Text;
            ini["POP Setting"]["SERVICE_NAME"] = txtsname.Text;
            ini["POP Setting"]["User id"] = txtid.Text;
            ini["POP Setting"]["Password"] = txtpassword.Text;
            ini.Save(@"C:\inifile\POP.ini");
            try
            {
                MessageBox.Show("POP setting 완료");
                this.Close();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Popset_Load(object sender, EventArgs e)
        {
            IniFile ini = new IniFile();
            ini.Load(@"C:\inifile\POP.ini");
            string POPhost = ini["POP setting"]["HOST"].ToString();
            string POPport = ini["POP Setting"]["PORT"].ToString();
            string POPsname = ini["POP Setting"]["SERVICE_NAME"].ToString();
            string POPid = ini["POP Setting"]["User Id"].ToString();
            string POPPassword = ini["POP Setting"]["Password"].ToString();

            txthost.Text = POPhost;
            txtport.Text = POPport;
            txtsname.Text = POPsname;
            txtid.Text = POPid;
            txtpassword.Text = POPPassword;
        }
    }
    
}
