﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace sunilDBmig
{
    public partial class Qmsset : Form
    {
        public Qmsset()
        {
            InitializeComponent();
        }

        private void btnQmsset_Click_1(object sender, EventArgs e)
        {
            IniFile ini = new IniFile();
            ini["QMS setting"]["HOST"] = txthost.Text;
            ini["QMS Setting"]["PORT"] = txtport.Text;
            ini["QMS Setting"]["SERVICE_NAME"] = txtsname.Text;
            ini["QMS Setting"]["User id"] = txtid.Text;
            ini["QMS Setting"]["Password"] = txtpassword.Text;
            ini.Save(@"C:\inifile\QMS.ini");
            try
            {
                MessageBox.Show("QMS setting 완료");
                this.Close();
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void Qmsset_Load(object sender, EventArgs e)
        {
            IniFile ini = new IniFile();
            ini.Load(@"C:\inifile\QMS.ini");
            string QMShost = ini["QMS setting"]["HOST"].ToString();
            string QMSport = ini["QMS Setting"]["PORT"].ToString();
            string QMSsname = ini["QMS Setting"]["SERVICE_NAME"].ToString();
            string QMSid = ini["QMS Setting"]["User Id"].ToString();
            string QMSPassword = ini["QMS Setting"]["Password"].ToString();

            txthost.Text = QMShost;
            txtport.Text = QMSport;
            txtsname.Text = QMSsname;
            txtid.Text = QMSid;
            txtpassword.Text = QMSPassword;
        }
    }
}
